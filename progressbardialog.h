#ifndef PROGRESSBARDIALOG_H
#define PROGRESSBARDIALOG_H

#include <QObject>
#include <QFutureWatcher>

class ProgressBarDialog : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float progress READ progress WRITE setProgress NOTIFY progressChanged)
public:
    ProgressBarDialog();
    float progress(int &iterator);
    // I will use this as a reference to pass to main.cpp using setContextProperty()
    QObject &refModel()
    {
        return m_Model;
    }

public Q_SLOTS:
    void startComputation();
    void cancelComputation();
signals:
    void progressChanged();
private:
    int m_progressValue;
    QObject m_Model;
    QFutureWatcher<void> m_futureWatcher;
};

#endif // PROGRESSBARDIALOG_H
